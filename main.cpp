/*SDL Practice application
 * Sean McArdle 12/2012 */


#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <SDL/SDL_mixer.h>
#include <SDL/SDL_image.h>
#include <string>


//Prototypes
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination,
            SDL_Rect* clip);
SDL_Surface *load_image( std::string filename );
bool init(void);
bool load_file(std::string filename);
int cleanUp(void);

SDL_Surface *screen = NULL;
SDL_Surface *dots = NULL;

SDL_Event event;

// The portions of the sprite make to be blitted
SDL_Rect clip[4];

//The attributes of the screen
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int SCREEN_BPP = 32;


int main( int argc, char **argv )
{
    //Ensure the application waits to quit
    bool quit = false;


    //Clip ranges
    //top left
    clip[0].x = 0;
    clip[0].y = 0;
    clip[0].w = 100;
    clip[0].h = 100;

    //top right
    clip[1].x = 100;
    clip[1].y = 0;
    clip[1].w = 100;
    clip[1].h = 100;

    //bottom left
    clip[2].x = 0;
    clip[2].y = 100;
    clip[2].w = 100;
    clip[2].h = 100;

    //bottom right
    clip[3].x = 100;
    clip[3].y = 100;
    clip[3].w = 100;
    clip[3].h = 100;

    //Initialize SDL
    if ( !init() ){
        return 1;
    }


    //Load image
    if ( !load_file( "images/dots.png" ) ){
        return 3;
    }

    //Fill the scren white
    SDL_FillRect( screen, &screen->clip_rect, SDL_MapRGB( screen->format, 0xFF,
                              0xFF, 0xFF));


    int dotX = 0;
    int dotY = 0;


    //Apply the image to the screen
    apply_surface( dotX, dotY, dots, screen, &clip[ 0 ] );
    apply_surface( 540, 0, dots, screen, &clip[ 1 ] );
    apply_surface( 0, 380, dots, screen, &clip[ 2 ] );
    apply_surface( 540, 380, dots, screen, &clip[ 3 ] );


    //Update the screen
    if( SDL_Flip( screen ) == -1 ){
    return 1;
    }

    //While the user hasn't quit
    while( quit == false ) {
        //While there's an event to handle
        while( SDL_PollEvent( &event ) ) {
            switch(event.type) {
                case SDL_QUIT:
                    quit = true;

                case SDL_KEYDOWN:
                    dotX = dotX + 10;
                    dotY = dotY + 10;
                    apply_surface( dotX, dotY, dots, screen, &clip[ 0 ] );
                    if( SDL_Flip( screen ) == -1 ) {
                        return 1;
                    }
                    break;
                case SDL_KEYUP:
                    dotX = dotX + 10;
                    dotY = dotY + 10;
                    apply_surface( dotX, dotY, dots, screen, &clip[ 0 ] );
                    if( SDL_Flip( screen ) == -1 ) {
                        return 1;
                    }
                    break;
            }
        }
    }


    return cleanUp();
}




int cleanUp()
{
    SDL_FreeSurface( dots );
    SDL_Quit();
    return 0;
}

bool init()
{
    //Start SDL
    if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 ) {
        return 1;
    }

    //Setup screen
    screen = SDL_SetVideoMode( SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE );

    //Catch error if the window couldn't be loaded
    if ( screen == NULL ) {
        return false;
    }

    //Window caption
    SDL_WM_SetCaption( "Hello World", NULL );

    //You made it this far, init must be ok.
    return true;
}

bool load_file( std::string filename )
{
    dots = load_image( filename );

    if ( dots == NULL ){
        return false;
    }

    return true;
}

//Load an image file (supports: png, jpg, bmp)
SDL_Surface *load_image( std::string filename )
{
    //Temp storage for texture
    SDL_Surface* loadedImage = NULL;

    //Optimized texture
    SDL_Surface* optimaizedImage = NULL;

    //Load image
    loadedImage = IMG_Load( filename.c_str() );

    if( loadedImage != NULL ) {
        //Create optimized image from loadedImage
        optimaizedImage = SDL_DisplayFormat( loadedImage );
        //Free up loadedImage
        SDL_FreeSurface( loadedImage );
    }

    return optimaizedImage;
}

//Paint a surface to another surface at a particular point
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL)
{
    //Make rectangle to hold the offsets
    SDL_Rect offset;

    offset.x = x;
    offset.y = y;

    //Blit surface
    SDL_BlitSurface( source, clip, destination, &offset );
}
